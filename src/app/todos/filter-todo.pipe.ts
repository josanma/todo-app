import { Pipe, PipeTransform } from '@angular/core';
import { Todo } from './models/todo.model';
import { validFilter } from './redux/filter/filter.actions';

@Pipe({
  name: 'filterTodo'
})
export class FilterTodoPipe implements PipeTransform {

  transform(todos: Todo[], filter: validFilter): Todo[] {
    console.log(todos, filter);
    switch (filter) {
      case 'ACTIVE':
        return todos.filter(todo => todo.completed === false);
        break;
      case 'COMPLETE':
        return todos.filter(todo => todo.completed === true);
        break;
      default :
        return todos;
    }
  }

}

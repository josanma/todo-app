import { Component, OnInit } from '@angular/core';
import { AppStates } from '../../app.states';
import { Todo } from '../models/todo.model';
import { Store } from '@ngrx/store';
import { validFilter } from '../redux/filter/filter.actions';


@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {

  todos: Todo[] = [];
  activeFilter: validFilter;

  constructor(private  store: Store<AppStates>) {
    this.store.subscribe(state => {
      this.todos = state.todos;
      this.activeFilter = state.filter;
    });
  }

  ngOnInit(): void {
  }

}

import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppStates } from '../../app.states';
import * as actionsFilter from '../redux/filter/filter.actions';
import * as actionsTodo from '../redux/todo.actions';

@Component({
  selector: 'app-todo-footer',
  templateUrl: './todo-footer.component.html',
  styleUrls: ['./todo-footer.component.css']
})
export class TodoFooterComponent implements OnInit {
  public activeFilter: actionsFilter.validFilter;
  public filters = [{key: 'ALL', value: 'Todas'}, {key: 'COMPLETE', value: 'Completadas'}, {key: 'ACTIVE', value: 'Activas'}];
  public pending: number;

  constructor(private store: Store<AppStates>) {
    this.pending = 0;
  }

  ngOnInit(): void {
    this.store.subscribe(state => {
      this.activeFilter = state.filter;
      this.pending = state.todos.filter(task => !task.completed).length;
    });
  }

  public filtering(filter: actionsFilter.validFilter) {
    this.store.dispatch(actionsFilter.filter({filterOpt: filter}));
  }

  public destroyCompleteTodo() {
    this.store.dispatch(actionsTodo.delComplete({}));
  }


}

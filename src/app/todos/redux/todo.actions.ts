import { createAction, props } from '@ngrx/store';

export const add = createAction('[TODO] add', props<{ task: string }>());
export const toggle = createAction('[TODO] toggle', props<{ id: number }>());
export const edit = createAction('[TODO] edit', props<{ id: number, task: string }>());
export const del = createAction('[TODO] del', props<{ id: number }>());
export const delComplete = createAction('[TODO] delComplete', props<{}>());
export const checkAll = createAction('[TODO] checkAll', props<{ checked: boolean }>());

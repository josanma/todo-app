import { createReducer, on } from '@ngrx/store';
import { add, checkAll, del, delComplete, edit, toggle } from './todo.actions';
import { Todo } from '../models/todo.model';

export const initialState: Todo[] = [];

function _toggleReducer(state, id: number) {
  return state.map(todo => {
    if (todo.id === id) {
      return {
        ...todo,
        completed: !todo.completed
      };
    } else {
      return todo;
    }
  });
}

function _editReducer(state, id: number, task: string) {
  return state.map(todo => {
    if (todo.id === id) {
      return {
        ...todo,
        task
      };
    } else {
      return todo;
    }
  });
}

function _checkAllReducer(todo, checked: boolean) {
  return {...todo, completed: checked};
}

const _todoReducer = createReducer(initialState,
  on(add, (state, {task}) => [...state, new Todo(task)]),
  on(toggle, (state, {id}) => _toggleReducer(state, id)),
  on(edit, (state, {id, task}) => _editReducer(state, id, task)),
  on(del, (state, {id}) => state.filter(todo => todo.id !== id)),
  on(delComplete, (state) => state.filter(todo => todo.completed !== true)),
  on(checkAll, (state, {checked}) => state.map(todo => _checkAllReducer(todo, checked))),
);

export function todoReducer(state, action): Todo[] {
  return _todoReducer(state, action);
}



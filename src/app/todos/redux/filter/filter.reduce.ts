import { createReducer, on } from '@ngrx/store';
import { filter, validFilter } from './filter.actions';

export const initialState: validFilter = 'ALL';

const _filterReducer = createReducer(initialState,
  on(filter, (state, {filterOpt}) => filterOpt),
);

export function filterReducer(state, action): validFilter {
  return _filterReducer(state, action);
}

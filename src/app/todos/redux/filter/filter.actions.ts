import { createAction, props } from '@ngrx/store';

export type validFilter = 'ALL' | 'ACTIVE' | 'COMPLETE';

export const filter = createAction('[TODO] filter', props<{ filterOpt: validFilter }>());

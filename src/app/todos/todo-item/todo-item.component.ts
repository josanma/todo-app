import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Todo } from '../models/todo.model';
import { FormControl, Validators } from '@angular/forms';
import * as actions from '../redux/todo.actions';
import { Store } from '@ngrx/store';
import { AppStates } from '../../app.states';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.css']
})
export class TodoItemComponent implements OnInit {
  @Input() todo: Todo;

  completedForm: FormControl;
  taskForm: FormControl;
  editing: boolean;

  @ViewChild('inputTask') inputTask: ElementRef;

  constructor(private  store: Store<AppStates>) {
    this.editing = false;
  }

  ngOnInit(): void {
    this.completedForm = new FormControl(this.todo.completed);
    this.taskForm = new FormControl(this.todo.task, Validators.required);
    this.completedForm.valueChanges.subscribe(() => this.store.dispatch(actions.toggle({id: this.todo.id})));
  }

  public edit(): void {
    this.editing = true;
    this.taskForm.setValue(this.todo.task);
    this._focus();
  }

  public endEdit(): void {
    this.editing = false;

    if (this.taskForm.invalid || this.taskForm.value === this.todo.task) {
      return;
    }

    this.store.dispatch(actions.edit({id: this.todo.id, task: this.taskForm.value}));
  }

  private _focus(): void {
    setTimeout(() => {
      this.inputTask.nativeElement.select();
    }, 0);
  }

  public destroy(): void {
    this.store.dispatch(actions.del({id: this.todo.id}));
  }

}

import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppStates } from '../app.states';
import * as actions from './redux/todo.actions';


@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  checkAllForm: FormControl;

  constructor(private store: Store<AppStates>) {
  }

  ngOnInit(): void {
    this.checkAllForm = new FormControl(false);
    this.checkAllForm.valueChanges.subscribe(isCheck => this.store.dispatch(actions.checkAll({checked: isCheck})));
  }

}

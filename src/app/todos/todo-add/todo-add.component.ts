import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppStates } from '../../app.states';
import * as actions from '../redux/todo.actions';

@Component({
  selector: 'app-todo-add',
  templateUrl: './todo-add.component.html',
  styleUrls: ['./todo-add.component.css']
})
export class TodoAddComponent implements OnInit {
  taskInput: FormControl;

  constructor(private  store: Store<AppStates>) {
    this.taskInput = new FormControl('', Validators.required);
  }

  ngOnInit(): void {
  }

  addTask(): void {
    if (this.taskInput.invalid) { return; }
    this.store.dispatch(actions.add({task: this.taskInput.value}));
  }

}

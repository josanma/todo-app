import { ActionReducerMap } from '@ngrx/store';
import { AppStates } from './app.states';
import { todoReducer } from './todos/redux/todo.reducer';
import { filterReducer } from './todos/redux/filter/filter.reduce';

export const appReducer: ActionReducerMap<AppStates> = {
  todos: todoReducer,
  filter: filterReducer
};

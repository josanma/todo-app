import { Todo } from './todos/models/todo.model';
import { validFilter } from './todos/redux/filter/filter.actions';

export interface AppStates {
  todos: Todo[];
  filter: validFilter;
}
